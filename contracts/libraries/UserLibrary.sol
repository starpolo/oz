// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;


library UserLibrary {
    
    struct User {
        uint64 tokenPrice;
        uint32 investmentCounter; 
        address referrer;
        uint8 lastIndex;
        bool KYC;
        uint32 bonusMatrixAllocation;
        uint64 id;
        uint128 totalTokensWithdrawn ;
        uint16 reffralCount;
        bool isInvested;
    }

    function exists(User storage self) internal view returns (bool) {
        return self.id != 0;
    }
}