// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;


library InvestmentLibrary {
  struct cycles {
    uint64 time;
    uint64 investment;
    uint128 tokens;
  } 
}   