// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "contracts/interfaces/IERC20.sol";
import "contracts/interfaces/OZT_Interface.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "contracts/upgradeability/CustomOwnable.sol";
import "contracts/libraries/UserLibrary.sol";
import "contracts/libraries/InvestmentLibrary.sol";
import "contracts/libraries/TransferHelper.sol";
import "hardhat/console.sol";


contract OZV0 is  CustomOwnable, ReentrancyGuard{
  using UserLibrary for UserLibrary.User;
  using InvestmentLibrary for InvestmentLibrary.cycles;
  IERC20 paymentToken;
  OZT_Interface OZT;
  
  
 
  uint256  private constant maxInvestmentCap = 3500;
  uint256 investmentLimitInDollars;
  uint256 DaysInSeconds;
  uint256  private tokenWorthDollars;
  uint256 private currentId;
  address public rootNode;
  address ozOperations;
  address OZTokenAddress;
  bool internal _initialized;
  uint8[] private reservedSlots;
  uint16[] private slotLimit;

  mapping(address => UserLibrary.User) public users;
  mapping(address =>bool) public tokenIsRegistered;
  mapping(address => mapping(uint64 => InvestmentLibrary.cycles)) public investment; 
  mapping(uint=>address) public idToAddress;

  event Registration(address indexed userAddress, address indexed referrerAddress, uint256 userId, uint256 referrerId,uint time);
  event Slotpurchased(address indexed userAddress,uint tokens,uint time,uint tokenPrice);
  event PurchaseTokens(address indexed userAddress, uint tokens,uint32 purchaseSlot,uint time,bool isReserved);
  event ReffralIncome(address user,address reffralAddress,uint reffralIncome,uint ozOperationIncome);

  // 0 - Ecozone slot
  // 1 - Core slot
  // 2 - ArchitectsSlot 
  // 3 - Influencers slot
 
  function initialize(address _root,address ozTokenAddress,address _ozOperations) external {
    require(!_initialized, 'OZV0: INVALID');
    _initialized = true;
    rootNode = _root;
    currentId = 1;
    DaysInSeconds = 1; 
    slotLimit = [1,8,80,800];
    reservedSlots = [0,0,0,0];
    OZT = OZT_Interface(ozTokenAddress);
    OZTokenAddress = ozTokenAddress;
    ozOperations = _ozOperations; 
    tokenWorthDollars = 3503500;
    saveDetailsOfUser(address(0),_root,true);
  }
  

  modifier onlyOwnerAccess() {
    require(msg.sender == rootNode,"OZV0: Only owner has the access");
    _;
  }


  function register(address _referrer,address user) public onlyOwnerAccess nonReentrant {
    require(!users[user].exists(),"OZV0:User already exists");
    require(users[_referrer].exists(),"OZV0:Referrer does not exists");
    saveDetailsOfUser(_referrer, user,true);
  }

  function saveDetailsOfUser (address _referrer,address user,bool KYC) private{
    users[user].id = uint64(currentId);
    users[user].referrer = _referrer;
    users[user].KYC = KYC;
    idToAddress[currentId] = user;
    users[_referrer].reffralCount++;
    emit Registration(user,_referrer,currentId,users[_referrer].id,block.timestamp);
    currentId++;
  }

  function addToken(address tokenAddress) public onlyOwnerAccess{
   // Sanity check tor token
    require(!(tokenIsRegistered[tokenAddress]),"OZV0:Token already exists");
    tokenIsRegistered[tokenAddress]= true;
  }
    

  function slotReservedByAdmin(address user,uint8 code) public onlyOwnerAccess nonReentrant {
    require((code>=0&&code<=3),"OZV0:Invalid allocation");
    require(users[user].exists(),"OZV0: User not registered");
    require((reservedSlots[code] < slotLimit[code]),"OZV0:Slot already purchased");
   // require(isSlotPurchased(user),"OZV0: User can only buy slot once");
    reservedSlots[code]++;
    uint16 slot;
    for(uint8 i;i< code;i++){
      slot += slotLimit[i];
    }
    slot += reservedSlots[code];
   // saveInvestmentDetails(user,uint8(currentTokenPrice()));
  }

  function _currentTokenPrice(uint256 _tokenWorthDollars) private pure returns(uint price){
    return (8 + ((_tokenWorthDollars/maxInvestmentCap)-1)*8);
  }

  function currentTokenPrice() public view returns(uint price){
    return (8 + ((tokenWorthDollars/maxInvestmentCap)-1)*8);
  }

  function purchaseTokens(uint256 value,address tokenAddress,address _referrer)public nonReentrant{
    require((tokenIsRegistered[tokenAddress]),"OZV0:Token is not  registered");
    paymentToken = IERC20(tokenAddress);
    uint paymentTokenDecimals = 10** paymentToken.decimals();
    //check for max investment
    require((value >= 100*paymentTokenDecimals),"OZV0: Investment less then 100$ is not allowed");
    if(value >= (maxInvestmentCap*paymentTokenDecimals)){
      require(((value) % (maxInvestmentCap*paymentTokenDecimals) ==  uint(0)),"OZV0 : Investment should be in multiple of 3500 dollars");
    }
    if((!(users[msg.sender].KYC)&& (users[msg.sender].investmentCounter == 0))){
      saveDetailsOfUser(_referrer, msg.sender, false);
    }
    if(users[msg.sender].KYC && users[_referrer].KYC && users[msg.sender].investmentCounter == 0){
      users[_referrer].bonusMatrixAllocation++;
    }
    TransferHelper.safeTransferFrom(tokenAddress, msg.sender, address(this), value);
    if(users[msg.sender].investmentCounter == 0){
      reffralBonus(value,paymentTokenDecimals,_referrer,msg.sender);
    }
    if(value < maxInvestmentCap*paymentTokenDecimals){
      console.log("reserved");
      invest(value,paymentTokenDecimals,msg.sender);
      
    }else{
      console.log("invested");
      //test case for investment 3500 after 100 dollar
      require(!(users[msg.sender].isInvested),"OZV0: Invalid investment");
     _reserveSlot(value,paymentTokenDecimals,msg.sender);
    }
  }

  function reffralBonus(uint256 value,uint paymentTokenDecimals,address _referrer,address user)private{
    uint ozOperationsamount = value/5;
    uint referralAmount;
    if(value < 300*paymentTokenDecimals){
      referralAmount = value/10;
    } else {
     referralAmount = 30*paymentTokenDecimals;
    }
    paymentToken.transfer(_referrer,referralAmount);
    paymentToken.transfer(ozOperations,ozOperationsamount-referralAmount);
    emit ReffralIncome(user,_referrer,referralAmount, ozOperationsamount-referralAmount);
  }


  function _reserveSlot(uint256 value,uint paymentTokenDecimals,address user) private  {
    uint n = (value/(maxInvestmentCap*paymentTokenDecimals));
    uint _tokens;
    uint _tokenWorthDollars = tokenWorthDollars;
    for(uint i=1;i<= n;i++){
      _tokens += (maxInvestmentCap*1000*10**(OZT.decimals()))/(_currentTokenPrice(_tokenWorthDollars)); 
      _tokenWorthDollars += (maxInvestmentCap);
      emit Slotpurchased(user,_tokens,block.timestamp,_currentTokenPrice(_tokenWorthDollars));
    }
    saveInvestmentDetails(user,_tokens,(value/paymentTokenDecimals));
    OZT.mint(address(this),_tokens);
    emit PurchaseTokens(user, _tokens,users[user].investmentCounter,block.timestamp,false);
  }
  

  function invest(uint256 value,uint paymentTokenDecimals,address user) private  {
    uint index;
    if(!users[user].isInvested){
      index =  users[user].investmentCounter+1;
    }else{
      index =  users[user].lastIndex;
    }
    uint investedValue;
    for(uint i = users[user].investmentCounter ;i > index ;i--){
      investedValue += (investment[user][uint64(i)].investment);
    }
    require((investedValue + (value/paymentTokenDecimals) <= maxInvestmentCap),"OZV0 : Invalid Investment");
    if(!users[user].isInvested){
      users[user].lastIndex =  uint8(users[user].investmentCounter);
      users[user].isInvested =  true;
      users[user].tokenPrice = uint64(_currentTokenPrice(tokenWorthDollars));
    } 
    else if((block.timestamp - investment[user][users[user].investmentCounter].time)/DaysInSeconds > 30){
      users[user].tokenPrice = uint64(_currentTokenPrice(tokenWorthDollars));
    }
    uint _tokens = (value*1000*10**(OZT.decimals()))/(users[user].tokenPrice*paymentTokenDecimals);
    saveInvestmentDetails(user,_tokens,(value/paymentTokenDecimals));
    OZT.mint(address(this),_tokens);
    if(investedValue + (value/paymentTokenDecimals) == maxInvestmentCap){
      users[user].isInvested =  false;
    }
    emit PurchaseTokens(user, _tokens,users[user].investmentCounter,block.timestamp,true);
  } 

  function saveInvestmentDetails(address user,uint _tokens,uint _tokenWorthDollars) private{
    users[user].investmentCounter++;
    investment[user][users[user].investmentCounter].tokens = uint128(_tokens) ;
    investment[user][users[user].investmentCounter].time = uint64(block.timestamp);
    investment[user][users[user].investmentCounter].investment = uint64(_tokenWorthDollars);
    tokenWorthDollars +=  _tokenWorthDollars;
  }

  function burnTokens(uint256 value) public onlyOwnerAccess {
    OZT.burn(value);
  } 

  function calculateDrip (address user) private  view  returns (uint) {
    uint daysOver;
    uint dailyIncome;
    uint drip;
  //  daysOver = (block.timestamp - (investment[addressToSlot[user]].time))/DaysInSeconds;
    if(daysOver > 365){
 //     dailyIncome = (investment[addressToSlot[user]].tokens)/(1460);
      if(daysOver<=1825){
        drip = dailyIncome*(daysOver-365);
      }else{
        drip = dailyIncome*(1460);
      }
      return drip;
    }else{
      return 0;
    }
  }

  function dripWithdrawal () public  nonReentrant {
    uint drip = _dripwithdrawal(msg.sender);
    OZT.transfer(msg.sender,drip); 
  }

  function _dripwithdrawal(address user) private returns(uint dripToTransfer) {
  //  require(!(isSlotPurchased(msg.sender)),"OZV0: Slot not purchased yet");
    uint drip = calculateDrip(user);
//    drip = drip-(investment[addressToSlot[user]].totalTokensWithdrawan);
    require(drip > 0 , "OZV0: Drip not generated yet");
    if(drip > OZT.balanceOf(address(this))){
      drip = OZT.balanceOf(address(this));
    }
//    investment[addressToSlot[user]].totalTokensWithdrawan += drip; 
    return drip;
  }

  function sellToken (address[]  memory tokenAddresses,uint [] memory numberOfTokens,uint value) public nonReentrant {
    require(users[msg.sender].exists(),"OZV0: User not registered");
    uint totalTokensInDollars;
    if(value==0){
      value = _dripwithdrawal(msg.sender);
    }else{
      require(OZT.balanceOf(msg.sender)>= value,"OZV0: Insufficient tokens");
      TransferHelper.safeTransferFrom(OZTokenAddress, msg.sender, address(this), value);
    }
    uint dollars = (value*_currentTokenPrice(tokenWorthDollars)*72)/1000000;
    for(uint i=0;i<=tokenAddresses.length-1;i++){
      paymentToken = IERC20(tokenAddresses[i]);
      require(paymentToken.balanceOf(address(this))>= numberOfTokens[i],"OZV0: Insufficient tokens");
      paymentToken.transfer(msg.sender, numberOfTokens[i]);
      totalTokensInDollars += paymentToken.decimals()* numberOfTokens[i];
    }
    require(dollars == totalTokensInDollars,"OZV0: Invalid input parameters");
  }
  
  
//   function getOZBonus () public{
//       require(users[msg.sender].bonusSlots >0,"OZV0:No bonus slot yet");
      
      
//   }
  
  



}

