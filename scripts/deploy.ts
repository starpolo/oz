import {
  OZV0,
  OZV0__factory,
  OwnedUpgradeabilityProxy,
  OwnedUpgradeabilityProxy__factory,
  StableTokens__factory,
  StableTokens,
  OZT__factory,
} from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { ethers } from "hardhat";
let impl: OZV0;
let OZ: OZV0;
let owner: SignerWithAddress;
let signers: SignerWithAddress[];
let proxy: OwnedUpgradeabilityProxy;
let erc20: StableTokens;

async function main() {
  let OZT: any;
  signers = await ethers.getSigners();
  owner = signers[0];

  proxy = await new OwnedUpgradeabilityProxy__factory(owner).deploy();
  impl = await new OZV0__factory(owner).deploy();
  OZ = await new OZV0__factory(owner).attach(proxy.address);
  OZT = await new OZT__factory(owner).deploy("OZToken", "OZT");
  erc20 = await new StableTokens__factory(owner).deploy("USDT", "USDT");
  const initializeData = impl.interface.encodeFunctionData("initialize", [
    owner.address,
    OZT.address,
    signers[1].address,
  ]);

  await proxy.upgradeToAndCall(impl.address, initializeData);

  console.log("OZ deployed on : ", OZ.address);
  console.log("Implementation deployed on : ", impl.address);
  console.log("USDT deployed on : ", erc20.address);
  console.log("OZT deployed on : ", OZT.address);
  console.log("Proxy deployed on : ", proxy.address);
}
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
