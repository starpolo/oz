// import {
//   OZV0,
//   OZV0__factory,
//   OwnedUpgradeabilityProxy,
//   OwnedUpgradeabilityProxy__factory,
//   StableTokens__factory,
//   OZT__factory,
// } from "../typechain";
// import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
// import { ethers } from "hardhat";
// import { mineBlocks } from "./utilities/utilities";
// import { expect } from "chai";

// describe("OZ", async () => {
//   let impl: OZV0;
//   let OZ: OZV0;
//   let owner: SignerWithAddress;
//   let signers: SignerWithAddress[];
//   let proxy: OwnedUpgradeabilityProxy;
//   let usdt: any;
//   let OZT: any;
//   beforeEach(async () => {
//     signers = await ethers.getSigners();
//     owner = signers[0];

//     proxy = await new OwnedUpgradeabilityProxy__factory(owner).deploy();
//     impl = await new OZV0__factory(owner).deploy();
//     OZ = await new OZV0__factory(owner).attach(proxy.address);
//     OZT = await new OZT__factory(owner).deploy("OZToken", "OZT");
//     const initializeData = impl.interface.encodeFunctionData("initialize", [
//       owner.address,
//       OZT.address,
//       signers[1].address,
//     ]);
//     await proxy.upgradeToAndCall(impl.address, initializeData);
//   });

//   describe("Token Deployment", async () => {
//     it("Deployment : Total supply matched", async () => {
//       usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
//       expect(await usdt.totalSupply()).to.be.eq("1000000000000000000000000");
//     });
//   });

//   describe("Registration", async () => {
//     it("Register: Fail, when registration tried by another user", async () => {
//       let user = signers[2];
//       await expect(
//         OZ.connect(user).register(owner.address, user.address)
//       ).to.be.revertedWith("OZV0: Only owner has the access");
//     });

//     it("Register: Fail, when a user is registered twice", async () => {
//       let user = signers[2];
//       await expect(OZ.connect(owner).register(owner.address, user.address));
//       await expect(
//         OZ.connect(owner).register(owner.address, user.address)
//       ).to.be.revertedWith("OZV0:User already exists");
//     });

//     it("Register: Fail, when referrer does not exists", async () => {
//       let user1 = signers[2];
//       let user2 = signers[3];
//       await expect(
//         OZ.connect(owner).register(user1.address, user2.address)
//       ).to.be.revertedWith("OZV0:Referrer does not exists");
//     });

//     it("Register: Success, user registered", async () => {
//       let user1 = signers[2];
//       let user2 = signers[3];
//       await OZ.connect(owner).register(owner.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       expect((await OZ.users(user2.address)).referrer).to.be.eq(user1.address);
//       expect((await OZ.users(user1.address)).referrer).to.be.eq(owner.address);
//     });

//     it("Register: Success,current id increased", async () => {
//       let user = signers[2];
//       await OZ.connect(owner).register(owner.address, user.address);
//       expect((await OZ.users(user.address)).id).to.be.eq(2);
//     });
//   });

//   describe("Invest in a Slot", async () => {
//     it("Investing in a slot: Fail, if a non-registered user tries to invest in a slot", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );

//       await expect(
//         OZ.connect(signers[20]).invest("500000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: User not registered");
//     });

//     it("Investing in a slot: Fail, if  user tries to invest in a random slot", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];
//       let user3 = signers[20];

//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       await OZ.connect(owner).register(user1.address, user3.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );

//       await expect(
//         OZ.connect(user3).invest("500000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: Slot not reserved yet");
//     });

//     it("Investing in a slot: Fail,if a user invests more then the price of slot", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "8000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "8000000000000000000000");
//       await expect(
//         OZ.connect(user2).invest("7600000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: Invalid investment");
//     });

//     it("Investing in a slot: Fail,if a user tries to invest in a slot with zero tokens", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "8000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "8000000000000000000000");
//       await expect(
//         OZ.connect(user2).invest("0", usdt.address)
//       ).to.be.revertedWith("OZV0: Insufficient investment");
//     });

//     it("Investing in a slot: Success", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];

//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       let currentPublicSlot = await OZ.addressToSlot(user2.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "8000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "8000000000000000000000");
//       await OZ.connect(user2).invest("2900000000000000000000", usdt.address);

//       await OZ.connect(user2).invest("100000000000000000000", usdt.address);
//       expect((await OZ.investment(currentPublicSlot)).tokens).to.be.eq(
//         "49157303370"
//       );
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("49157303370");
//     });

//     it("Invest in a slot allocated by owner : Success", async () => {
//       let tokenOwner = signers[2];
//       let user2 = signers[3];
//       usdt = await new StableTokens__factory(tokenOwner).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(tokenOwner)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await OZ.connect(owner).register(owner.address, user2.address);
//       await OZ.connect(owner).slotReservedByAdmin(user2.address, 1);
//       await OZ.connect(user2).invest("3500000000000000000000", usdt.address);
//       let currentSlot = await OZ.addressToSlot(user2.address);
//       expect((await OZ.investment(currentSlot)).slotPrice.toString()).to.be.eq(
//         "16"
//       );
//       expect((await OZ.investment(currentSlot)).tokens).to.be.eq(
//         "21875000000000"
//       );
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("21875000000000");
//     });
//   });

//   describe("Token Registration", async () => {
//     it("Token Register : Fail, if a user other then owner tries to register a token", async () => {
//       let user1 = signers[2];
//       usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
//       await expect(OZ.connect(user1).addToken(usdt.address)).to.be.revertedWith(
//         "OZV0: Only owner has the access"
//       );
//     });

//     it("Token Register : Fail, if token is already registered", async () => {
//       usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await expect(OZ.connect(owner).addToken(usdt.address)).to.be.revertedWith(
//         "OZV0:Token already exists"
//       );
//     });

//     it("Token Register : Success", async () => {
//       usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       expect(await OZ.tokenIsRegistered(usdt.address)).to.be.eq(true);
//     });
//   });

//   describe("Check token price", async () => {
//     it("Token price matched: Success", async () => {
//       expect(await OZ.currentTokenPrice()).to.be.eq("8008");
//     });
//   });

//   describe("Allocation of slots by admin", async () => {
//     it("Allocation : Failed,if a user other then owner tries to allocate slots", async () => {
//       let user = signers[2];
//       await expect(
//         OZ.connect(user).slotReservedByAdmin(user.address, 0)
//       ).to.be.revertedWith("OZV0: Only owner has the access");
//     });
//     it("Allocation : Failed,if invaid code is passed", async () => {
//       let user = signers[2];
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 5)
//       ).to.be.revertedWith("OZV0:Invalid allocation");
//     });
//     it("Allocation : Failed,if user not registered", async () => {
//       let user = signers[2];
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 0)
//       ).to.be.revertedWith("OZV0: User not registered");
//     });
//     it("Allocation : Failed,if slot already purchased", async () => {
//       let user = signers[2];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).slotReservedByAdmin(user.address, 0);
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 0)
//       ).to.be.revertedWith("OZV0:Slot already purchased");
//       expect((await OZ.investment(1)).slotPrice.toString()).to.be.eq("8");
//       for (let i = 1; i <= 8; i++) {
//         await OZ.connect(owner).register(owner.address, signers[i + 2].address);
//         await OZ.connect(owner).slotReservedByAdmin(
//           signers[i + 2].address,
//           "1"
//         );
//       }
//       expect((await OZ.investment(2)).slotPrice.toString()).to.be.eq("16");
//       expect((await OZ.investment(9)).slotPrice.toString()).to.be.eq("72");
//       await OZ.connect(owner).register(owner.address, signers[80].address);
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(signers[80].address, 1)
//       ).to.be.revertedWith("OZV0:Slot already purchased");
//     });

//     it("Allocation : Failed,if a user allocated with more then one slot", async () => {
//       let user = signers[2];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).slotReservedByAdmin(user.address, 1);
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 1)
//       ).to.be.revertedWith("OZV0: User can only buy slot once");
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 2)
//       ).to.be.revertedWith("OZV0: User can only buy slot once");
//     });
//     it("Allocation : Success", async () => {
//       let user = signers[2];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).slotReservedByAdmin(user.address, 0);
//       await expect(
//         OZ.connect(owner).slotReservedByAdmin(user.address, 0)
//       ).to.be.revertedWith("OZV0:Slot already purchased");
//       expect((await OZ.investment(1)).slotPrice.toString()).to.be.eq("8");
//       for (let i = 3; i <= 13; i++) {
//         await OZ.connect(owner).register(owner.address, signers[i].address);
//         await OZ.connect(owner).slotReservedByAdmin(signers[i].address, "2");
//       }
//       expect(
//         (
//           await OZ.investment(await OZ.addressToSlot(signers[3].address))
//         ).slotPrice.toString()
//       ).to.be.eq("80");
//       expect(
//         (
//           await OZ.investment(await OZ.addressToSlot(signers[12].address))
//         ).slotPrice.toString()
//       ).to.be.eq("152");
//       expect(
//         (
//           await OZ.investment(await OZ.addressToSlot(signers[13].address))
//         ).slotPrice.toString()
//       ).to.be.eq("160");
//       expect(
//         (
//           await OZ.investment(await OZ.addressToSlot(signers[14].address))
//         ).slotPrice.toString()
//       ).to.be.eq("0");
//     });
//   });

//   describe("Reserve Slot", async () => {
//     it("Reserving Slot: Fail, if a user is not registered", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user1.address, "500000000000000000000");
//       await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
//       await expect(
//         OZ.connect(user1).reserveSlot("500000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: User not registered");
//     });

//     it("Reserving Slot: Fail, if token is not registered", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).register(owner.address, user1.address);
//       await usdt.connect(user).transfer(user1.address, "500000000000000000000");
//       await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
//       await expect(
//         OZ.connect(user1).reserveSlot("500000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0:Token is not  registered");
//     });

//     it("Reserving Slot: Fail, if a user tries buy slot more then once", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await OZ.connect(owner).register(owner.address, user1.address);
//       await usdt.connect(user).transfer(user1.address, "500000000000000000000");
//       await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
//       await OZ.connect(user1).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       await expect(
//         OZ.connect(user1).reserveSlot("500000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: User can only buy slot once");
//     });

//     it("Reserving Slot: Fail, if investment is less then 100$", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await OZ.connect(owner).register(owner.address, user1.address);
//       await usdt.connect(user).transfer(user1.address, "500000000000000000000");
//       await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
//       await expect(
//         OZ.connect(user1).reserveSlot("90000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: Invalid investment");
//     });

//     it("Reserving Slot: Fail,if a user purchased a complete slot,and then tries to invest in it", async () => {
//       let user = signers[2];
//       let user2 = signers[3];
//       await OZ.connect(owner).register(owner.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "3500000000000000000000",
//         usdt.address
//       );
//       await expect(
//         OZ.connect(user2).invest("1000000000000000000000", usdt.address)
//       ).to.be.revertedWith("OZV0: Invalid investment");
//     });

//     it("Reserving Slot: Success", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];

//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(
//         "500000000000000000000"
//       );
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       let currentPublicSlot = await OZ.addressToSlot(user2.address);
//       let newSlot = await OZ.currentPublicSlot();
//       expect(await usdt.balanceOf(user2.address)).to.be.eq(0);
//       expect(await usdt.balanceOf(user1.address)).to.be.eq(
//         "30000000000000000000"
//       );
//       expect(await usdt.balanceOf(signers[1].address)).to.be.eq(
//         "70000000000000000000"
//       );
//       expect((await OZ.investment(currentPublicSlot)).tokens).to.be.eq(
//         "7022471910"
//       );
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("7022471910");
//       expect(newSlot).to.be.eq(currentPublicSlot + 1);
//     });

//     it("Reserving Slot: Success,if a user purchased a full slot", async () => {
//       let user = signers[2];
//       let user2 = signers[3];

//       await OZ.connect(owner).register(owner.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "3500000000000000000000",
//         usdt.address
//       );
//       let currentPublicSlot = await OZ.addressToSlot(user2.address);
//       expect((await OZ.investment(currentPublicSlot)).tokens).to.be.eq(
//         "49157303370"
//       );
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("49157303370");
//     });

//     it("Matching reffral bonus and ozoperations balance: Success , if investment is under 300$", async () => {
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];
//       let ozOperations = signers[1];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "100000000000000000000",
//         usdt.address
//       );
//       expect(await usdt.balanceOf(user1.address)).to.be.eq(
//         "10000000000000000000"
//       );
//       expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
//         "10000000000000000000"
//       );
//     });

//     it("Matching reffral bonus and ozoperations balance: Success , if investment is greater then 300$", async () => {
//       let ozOperations = signers[1];
//       let user = signers[2];
//       let user1 = signers[3];
//       let user2 = signers[4];

//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       expect(await usdt.balanceOf(user1.address)).to.be.eq(
//         "30000000000000000000"
//       );
//       expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
//         "70000000000000000000"
//       );
//     });
//   });

//   describe("Withdraw drip", async () => {
//     it("Drip withdrawal: Failure, if no slot is purchased by user", async () => {
//       let user = signers[2];
//       let user2 = signers[3];

//       await OZ.connect(owner).register(owner.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await expect(OZ.connect(user2).dripWithdrawal()).to.be.revertedWith(
//         "OZV0: Slot not purchased yet"
//       );
//     });

//     it("Drip withdrawal: Failure, drip is zero", async () => {
//       let user = signers[2];
//       let user2 = signers[3];

//       await OZ.connect(owner).register(owner.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "3500000000000000000000",
//         usdt.address
//       );
//       await mineBlocks(ethers.provider, 364);
//       await expect(OZ.connect(user2).dripWithdrawal()).to.be.revertedWith(
//         "OZV0: Drip not generated yet"
//       );
//     });

//     it("Drip withdrawal: Success", async () => {
//       let user = signers[2];
//       let user2 = signers[3];

//       await OZ.connect(owner).register(owner.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt
//         .connect(user)
//         .transfer(user2.address, "5000000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "3500000000000000000000",
//         usdt.address
//       );
//       let slot = await OZ.addressToSlot(user2.address);
//       await mineBlocks(ethers.provider, 366);
//       expect((await OZ.investment(slot)).totalTokensWithdrawan).to.be.eq("0");
//       await OZ.connect(user2).dripWithdrawal();
//       expect((await OZ.investment(slot)).totalTokensWithdrawan).to.be.eq(
//         "33669385"
//       );
//       expect(await OZT.balanceOf(user2.address)).to.be.eq("33669385");
//       await mineBlocks(ethers.provider, 2);
//       await OZ.connect(user2).dripWithdrawal();
//       expect((await OZ.investment(slot)).totalTokensWithdrawan).to.be.eq(
//         "101008155"
//       );
//       expect(await OZT.balanceOf(user2.address)).to.be.eq("101008155");
//       await mineBlocks(ethers.provider, 2000);
//       await OZ.connect(user2).dripWithdrawal();
//       expect((await OZ.investment(slot)).totalTokensWithdrawan).to.be.eq(
//         "49157302100"
//       );
//       expect(await OZT.balanceOf(user2.address)).to.be.eq("49157302100");
//     });
//   });

//   describe("Burn Tokens", async () => {
//     it("Burning Tokens: Fail, if tried by a user other then owner", async () => {
//       let user = signers[6];
//       await expect(
//         OZ.connect(user).burnTokens("500000000000000000000")
//       ).to.be.revertedWith("OZV0: Only owner has the access");
//     });
//     it("Burning Tokens: Fail,if amount is greater then contract balance ", async () => {
//       let user = signers[6];
//       await expect(
//         OZ.connect(owner).burnTokens("500000000000000000000")
//       ).to.be.revertedWith("ERC20: burn amount exceeds balance");
//     });

//     it("Burning Tokens: Success ", async () => {
//       let user = signers[6];
//       let user1 = signers[7];
//       let user2 = signers[8];
//       await OZ.connect(owner).register(owner.address, user.address);
//       await OZ.connect(owner).register(user.address, user1.address);
//       await OZ.connect(owner).register(user1.address, user2.address);
//       usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
//       await OZ.connect(owner).addToken(usdt.address);
//       await usdt.connect(user).transfer(user2.address, "500000000000000000000");
//       await usdt.connect(user2).approve(OZ.address, "500000000000000000000");
//       await OZ.connect(user2).reserveSlot(
//         "500000000000000000000",
//         usdt.address
//       );
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("7022471910");
//       await OZ.burnTokens("3511235955");
//       expect(await OZT.balanceOf(OZ.address)).to.be.eq("3511235955");
//     });
//   });
// });
