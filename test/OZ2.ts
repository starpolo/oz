import {
  OZV0,
  OZV0__factory,
  OwnedUpgradeabilityProxy,
  OwnedUpgradeabilityProxy__factory,
  StableTokens__factory,
  OZT__factory,
} from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { ethers } from "hardhat";
import { mineBlocks } from "./utilities/utilities";
import { expect } from "chai";

describe("OZ", async () => {
  let impl: OZV0;
  let OZ: OZV0;
  let owner: SignerWithAddress;
  let signers: SignerWithAddress[];
  let proxy: OwnedUpgradeabilityProxy;
  let usdt: any;
  let OZT: any;
  beforeEach(async () => {
    signers = await ethers.getSigners();
    owner = signers[0];

    proxy = await new OwnedUpgradeabilityProxy__factory(owner).deploy();
    impl = await new OZV0__factory(owner).deploy();
    OZ = await new OZV0__factory(owner).attach(proxy.address);
    OZT = await new OZT__factory(owner).deploy("OZToken", "OZT");
    const initializeData = impl.interface.encodeFunctionData("initialize", [
      owner.address,
      OZT.address,
      signers[1].address,
    ]);
    await proxy.upgradeToAndCall(impl.address, initializeData);
  });

  describe("Token Deployment", async () => {
    it("Deployment : Total supply matched", async () => {
      usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
      expect(await usdt.totalSupply()).to.be.eq("1000000000000000000000000");
    });
  });

  describe("Registration", async () => {
    it("Register: Fail, when registration tried by another user", async () => {
      let user = signers[2];
      await expect(
        OZ.connect(user).register(owner.address, user.address)
      ).to.be.revertedWith("OZV0: Only owner has the access");
    });

    it("Register: Fail, when a user is registered twice", async () => {
      let user = signers[2];
      await expect(OZ.connect(owner).register(owner.address, user.address));
      await expect(
        OZ.connect(owner).register(owner.address, user.address)
      ).to.be.revertedWith("OZV0:User already exists");
    });

    it("Register: Fail, when referrer does not exists", async () => {
      let user1 = signers[2];
      let user2 = signers[3];
      await expect(
        OZ.connect(owner).register(user1.address, user2.address)
      ).to.be.revertedWith("OZV0:Referrer does not exists");
    });

    it("Register: Success, user registered", async () => {
      let user1 = signers[2];
      let user2 = signers[3];
      let user3 = signers[4];
      await OZ.connect(owner).register(owner.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      await OZ.connect(owner).register(user1.address, user3.address);
      expect((await OZ.users(user2.address)).referrer).to.be.eq(user1.address);
      expect((await OZ.users(user1.address)).referrer).to.be.eq(owner.address);
      expect((await OZ.users(user2.address)).KYC).to.be.eq(true);
      expect((await OZ.users(owner.address)).reffralCount).to.be.eq(1);
      expect((await OZ.users(user1.address)).reffralCount).to.be.eq(2);
    });

    it("Register: Success,current id increased", async () => {
      let user = signers[2];
      await OZ.connect(owner).register(owner.address, user.address);
      expect((await OZ.users(user.address)).id).to.be.eq(2);
    });
  });

  describe("Token Registration", async () => {
    it("Token Register : Fail, if a user other then owner tries to register a token", async () => {
      let user1 = signers[2];
      usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
      await expect(OZ.connect(user1).addToken(usdt.address)).to.be.revertedWith(
        "OZV0: Only owner has the access"
      );
    });

    it("Token Register : Fail, if token is already registered", async () => {
      usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await expect(OZ.connect(owner).addToken(usdt.address)).to.be.revertedWith(
        "OZV0:Token already exists"
      );
    });

    it("Token Register : Success", async () => {
      usdt = await new StableTokens__factory(owner).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      expect(await OZ.tokenIsRegistered(usdt.address)).to.be.eq(true);
    });
  });

  describe("Check token price", async () => {
    it("Token price matched: Success", async () => {
      expect(await OZ.currentTokenPrice()).to.be.eq("8008");
    });
  });

  describe("Purchase Tokens", async () => {
    it("Purchasing Tokens: Fail, if token is not registered", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0:Token is not  registered");
    });

    it("Purchasing Tokens: Fail, if investment is less then 100$", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "90000000000000000000",
          usdt.address,
          user.address
        )
      ).to.be.revertedWith("OZV0: Investment less then 100$ is not allowed");
    });

    it("Purchasing Tokens: Fail , if investment is not a multiple of 3500$", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      let ozOperations = signers[1];
      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt
        .connect(user)
        .transfer(user2.address, "5000000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");

      await expect(
        OZ.connect(user2).purchaseTokens(
          "3600000000000000000000",
          usdt.address,
          user.address
        )
      ).to.be.revertedWith(
        "OZV0 : Investment should be in multiple of 3500 dollars"
      );
    });

    it("Purchasing Tokens: Success , if investment is greater then 100$ or is 100$", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt
        .connect(user)
        .transfer(user2.address, "5000000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");

      await OZ.connect(user2).purchaseTokens(
        "700000000000000000000",
        usdt.address,
        user.address
      );
    });

    it("Purchasing Tokens: Success, User can invest without KYC", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(0);
      expect((await OZ.users(user1.address)).id).to.be.eq("0");

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(1);
      expect((await OZ.users(user1.address)).id).to.be.eq("2");
      expect((await OZ.users(user1.address)).referrer).to.be.eq(user.address);
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(2);
      expect((await OZ.users(user1.address)).id).to.be.eq("2");
      expect((await OZ.users(user1.address)).referrer).to.be.eq(user.address);
    });
    it("Purchasing Tokens: Success, User can invest after doing  KYC", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).register(owner.address, user1.address);
      await OZ.connect(owner).addToken(usdt.address);
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      expect((await OZ.users(user1.address)).id).to.be.eq("2");
      expect((await OZ.users(user1.address)).referrer).to.be.eq(owner.address);

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );

      expect((await OZ.users(user1.address)).id).to.be.eq("2");
      expect((await OZ.users(user1.address)).referrer).to.be.eq(owner.address);
    });
    it("Purchasing Tokens: Success, bonus tokens not alloted to referrer if user or referr is not registered", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      await OZ.connect(owner).register(owner.address, user.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user).transfer(user2.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "500000000000000000000");

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );
      expect((await OZ.users(user.address)).bonusMatrixAllocation).to.be.eq(0);
    });

    it("Purchasing Tokens: Success, bonus tokens allocation only once per referrer, if user and referrer have completed there KYC", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt.connect(user).transfer(user1.address, "500000000000000000000");
      await usdt.connect(user).transfer(user2.address, "500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "500000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "500000000000000000000");

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );
      await OZ.connect(user2).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user.address
      );

      expect((await OZ.users(user.address)).bonusMatrixAllocation).to.be.eq(2);
    });

    it("Purchasing Tokens: Success,reffaral bonus and OZoperations amount matched if investment is under 300$", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      let ozOperations = signers[1];
      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt
        .connect(user)
        .transfer(user2.address, "5000000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "5000000000000000000000");
      await OZ.connect(user2).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect(await usdt.balanceOf(user1.address)).to.be.eq(
        "10000000000000000000"
      );
      expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
        "10000000000000000000"
      );
    });

    it("Purchasing Tokens : Success ,referr bonus goes only one time to refererr,if investment is done multiple times in multiple of 3500$", async () => {
      let ozOperations = signers[1];
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];

      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt
        .connect(user)
        .transfer(user2.address, "7000000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "7000000000000000000000");
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(0);
      await OZ.connect(user2).purchaseTokens(
        "3500000000000000000000",
        usdt.address,
        user1.address
      );
      expect(await usdt.balanceOf(user1.address)).to.be.eq(
        "30000000000000000000"
      );
      expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
        "670000000000000000000"
      );
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(1);
      await OZ.connect(user2).purchaseTokens(
        "3500000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(2);
      expect(await usdt.balanceOf(user1.address)).to.be.eq(
        "30000000000000000000"
      );
      expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
        "670000000000000000000"
      );
    });

    it("Purchasing Tokens :  Success ,referr bonus goes only one time to refererr,if investment is done within 3500$", async () => {
      let ozOperations = signers[1];
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];

      await OZ.connect(owner).register(owner.address, user.address);
      await OZ.connect(owner).register(user.address, user1.address);
      await OZ.connect(owner).register(user1.address, user2.address);
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);
      await usdt
        .connect(user)
        .transfer(user2.address, "7000000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "7000000000000000000000");
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(0);
      await OZ.connect(user2).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect(await usdt.balanceOf(user1.address)).to.be.eq(
        "10000000000000000000"
      );
      expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
        "10000000000000000000"
      );
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(1);
      await OZ.connect(user2).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user2.address)).investmentCounter).to.be.eq(2);
      expect(await usdt.balanceOf(user1.address)).to.be.eq(
        "10000000000000000000"
      );
      expect(await usdt.balanceOf(ozOperations.address)).to.be.eq(
        "10000000000000000000"
      );
    });
  });

  describe("Purchasing Tokens internally", async () => {
    it("Purchasing Tokens: Success, if invested in multiples of 3500$ ", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);

      await usdt
        .connect(user)
        .transfer(user1.address, "10500000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "10500000000000000000000");
      await OZ.connect(user1).purchaseTokens(
        "10500000000000000000000",
        usdt.address,
        user1.address
      );

      expect((await OZ.investment(user1.address, 1)).tokens).to.be.eq(
        "130988110928"
      );
    });

    it("Purchasing Tokens: Fail, if a user a user reserves token some tokens,and tries to buy more at same price", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);

      await usdt
        .connect(user)
        .transfer(user1.address, "21000000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "21000000000000000000000");
      await OZ.connect(user1).purchaseTokens(
        "7000000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(1);
      expect((await OZ.investment(user1.address, 1)).tokens).to.be.eq(
        "87368968356"
      );
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );

      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(2);
    });

    it("Purchasing Tokens: Success, if a user a user reserves token tokens,and tries to buy remaining at same price", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);

      await usdt
        .connect(user)
        .transfer(user1.address, "21000000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "21000000000000000000000");
      await OZ.connect(user1).purchaseTokens(
        "7000000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(1);
      expect((await OZ.investment(user1.address, 1)).tokens).to.be.eq(
        "87368968356"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(2);
      expect((await OZ.investment(user1.address, 2)).tokens).to.be.eq(
        "1246261216"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      await OZ.connect(user1).purchaseTokens(
        "200000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(3);
      expect((await OZ.investment(user1.address, 3)).tokens).to.be.eq(
        "2492522432"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
      await OZ.connect(user1).purchaseTokens(
        "600000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(4);
      expect((await OZ.investment(user1.address, 4)).tokens).to.be.eq(
        "7477567298"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      await OZ.connect(user1).purchaseTokens(
        "2600000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(5);
      expect((await OZ.investment(user1.address, 5)).tokens).to.be.eq(
        "32402791625"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8032");
      await OZ.connect(user1).purchaseTokens(
        "7000000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(6);
      expect((await OZ.investment(user1.address, 6)).tokens).to.be.eq(
        "87108035519"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8048");
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(7);
      expect((await OZ.investment(user1.address, 7)).tokens).to.be.eq(
        "1242544731"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8048");
    });

    it("Purchasing Tokens: Success,if user first reserves token at some price, then purchases token and then try to invest in slots of 3500$", async () => {
      let user = signers[2];
      let user1 = signers[3];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);

      await usdt
        .connect(user)
        .transfer(user1.address, "21000000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "21000000000000000000000");

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(1);
      expect((await OZ.investment(user1.address, 1)).tokens).to.be.eq(
        "1248751248"
      );

      expect(await OZ.currentTokenPrice()).to.be.eq("8008");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      await OZ.connect(user1).purchaseTokens(
        "200000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(2);
      expect((await OZ.investment(user1.address, 2)).tokens).to.be.eq(
        "2497502497"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8008");
      await OZ.connect(user1).purchaseTokens(
        "600000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(3);
      expect((await OZ.investment(user1.address, 3)).tokens).to.be.eq(
        "7492507492"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8008");
      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      await OZ.connect(user1).purchaseTokens(
        "2600000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(4);
      expect((await OZ.investment(user1.address, 4)).tokens).to.be.eq(
        "32467532467"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8016");
      await OZ.connect(user1).purchaseTokens(
        "7000000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(5);
      expect((await OZ.investment(user1.address, 5)).tokens).to.be.eq(
        "87281817222"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8032");
      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(6);
      expect((await OZ.investment(user1.address, 6)).tokens).to.be.eq(
        "1245019920"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8032");
    });
    it("Purchasing Tokens: Success,if a user invests in a reserved slot after 30 days ,he gets token at a new price", async () => {
      let user = signers[2];
      let user1 = signers[3];
      let user2 = signers[4];
      let user3 = signers[5];
      usdt = await new StableTokens__factory(user).deploy("USDT", "USDT");
      await OZ.connect(owner).addToken(usdt.address);

      await usdt
        .connect(user)
        .transfer(user1.address, "21000000000000000000000");
      await usdt.connect(user1).approve(OZ.address, "21000000000000000000000");
      await usdt
        .connect(user)
        .transfer(user2.address, "3500000000000000000000");
      await usdt.connect(user2).approve(OZ.address, "3500000000000000000000");
      await usdt
        .connect(user)
        .transfer(user3.address, "3500000000000000000000");
      await usdt.connect(user3).approve(OZ.address, "3500000000000000000000");

      await OZ.connect(user1).purchaseTokens(
        "100000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(1);
      expect((await OZ.investment(user1.address, 1)).tokens).to.be.eq(
        "1248751248"
      );

      expect(await OZ.currentTokenPrice()).to.be.eq("8008");
      await mineBlocks(ethers.provider, 29);
      await expect(
        OZ.connect(user1).purchaseTokens(
          "3500000000000000000000",
          usdt.address,
          user1.address
        )
      ).to.be.revertedWith("OZV0: Invalid investment");
      await OZ.connect(user2).purchaseTokens(
        "3500000000000000000000",
        usdt.address,
        user.address
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8016");
      await OZ.connect(user1).purchaseTokens(
        "200000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(2);
      expect((await OZ.investment(user1.address, 2)).tokens).to.be.eq(
        "2497502497"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8016");
      await mineBlocks(ethers.provider, 32);
      await OZ.connect(user3).purchaseTokens(
        "3500000000000000000000",
        usdt.address,
        user.address
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
      await OZ.connect(user1).purchaseTokens(
        "600000000000000000000",
        usdt.address,
        user1.address
      );
      expect((await OZ.users(user1.address)).investmentCounter).to.be.eq(3);
      expect((await OZ.investment(user1.address, 3)).tokens).to.be.eq(
        "7477567298"
      );
      expect(await OZ.currentTokenPrice()).to.be.eq("8024");
    });
  });
});
